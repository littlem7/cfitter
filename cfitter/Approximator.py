"""
(contains some approximate functions possible to fit and present using cfitter module)
"""
import cfitter.Fitter as F
import cfitter.StatAnalyser as SA
import cfitter.Plotter as P
import numpy as np
from scipy import signal


def approximate_with_all(name_of_file, show = False, x_label = 'x value []', y_label = 'y value []', main_title = 'name_of_chart'):
    """
    function causes all approximations
    """
    approximate_with_algebraic_polynomial(name_of_file, show, x_label, y_label, main_title + ' with algebraic polynomial approximation')
    approximate_with_linear(name_of_file, show, x_label, y_label, main_title + ' with linear approximation')
    approximate_with_exponential(name_of_file, show, x_label, y_label, main_title + ' with exponential approximation')
    approximate_with_trigonometric_polynomial(name_of_file, show, x_label, y_label, main_title + ' with trigonometric polynomial spproximation')


def presentation_of_approximation(name_of_file, fitted_function, show = False, name_of_app = '[type of approximation]', x_label = 'x value []', y_label = 'y value []', name_of_chart = 'name_of_chart'):
    """
    :param name_of_file: name of file without enlargement where data is
    :param fitted_function: function fit to
    :param show: relate to show method connected with Plotter
    :param name_of_app: name of special approximation
    :param x_label: name of x label on plot
    :param y_label: name of y label on plot
    :param name_of_chart: special name of plot
    """
    print('Result of chi square test for %s approximation' % name_of_app)
    chi, p_val = SA.chi_square_test(name_of_file, fitted_function)
    P.make_plot_with_function(name_of_file, fitted_function, show, x_label, y_label, name_of_chart + ' χ²: %0.1f, p: %0.1f' % (chi, p_val))

"""
possible approximations:
- algebraic polynomial
- linear
- trigonometric polynomial
- exponential (gaussian)
"""


def app_algebraic_polynomial(x, a0, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10):
    result = 0
    result += a10*np.power(x, 10)
    result += a9*np.power(x, 9)
    result += a8*np.power(x, 8)
    result += a7*np.power(x, 7)
    result += a6*np.power(x, 6)
    result += a5*np.power(x, 5)
    result += a4*np.power(x, 4)
    result += a3*np.power(x, 3)
    result += a2*np.power(x, 2)
    result += a1*np.power(x, 1)
    result += a0*np.power(x, 0)
    return result


def approximate_with_algebraic_polynomial(name_of_file, show = False, x_label = 'x value []', y_label = 'y value []', name_of_chart = 'name_of_chart'):
    fv = F.fit_data_from_file(name_of_file, app_algebraic_polynomial)[0]

    def fitted_function(x):
        return app_algebraic_polynomial(x, fv[0], fv[1], fv[2], fv[3], fv[4], fv[5], fv[6], fv[7], fv[8], fv[9], fv[10])
    presentation_of_approximation(name_of_file, fitted_function, show, 'algebraic polynomial', x_label, y_label, name_of_chart)


def app_linear(x, a, b):
    return app_algebraic_polynomial(x, a, b, 0, 0, 0, 0, 0, 0, 0, 0, 0)


def approximate_with_linear(name_of_file, show = False, x_label = 'x value []', y_label = 'y value []', name_of_chart = 'name_of_chart'):
    fv = F.fit_data_from_file(name_of_file, app_linear)[0]

    def fitted_function(x):
        return app_linear(x, fv[0], fv[1])
    presentation_of_approximation(name_of_file, fitted_function, show, 'linear', x_label, y_label, name_of_chart)


def app_trigonometric_polynomial(x, a0, a, b, c, d):
    return a*np.cos(b*x) + c*np.sin(d*x) + a0*np.power(x, 0)


def approximate_with_trigonometric_polynomial(name_of_file, show = False, x_label = 'x value []', y_label = 'y value []', name_of_chart = 'name_of_chart'):
    fv = F.fit_data_from_file(name_of_file, app_trigonometric_polynomial)[0]

    def fitted_function(x):
        return app_trigonometric_polynomial(x, fv[0], fv[1], fv[2], fv[3], fv[4])
    presentation_of_approximation(name_of_file, fitted_function, show, 'trigonometric polynomial', x_label, y_label, name_of_chart)


def app_exponential_function(x, a, b, c):
    return signal.gaussian(100, std=b)[x]*(a - 20) + c


def approximate_with_exponential(name_of_file, show = False, x_label = 'x value []', y_label = 'y value []', name_of_chart = 'name_of_chart'):
    fv = F.fit_data_from_file(name_of_file, app_exponential_function)[0]

    def fitted_function(x):
        return app_exponential_function(x, fv[0], fv[1], fv[2])
    presentation_of_approximation(name_of_file, fitted_function, show, 'exponential (gaussian)', x_label, y_label, name_of_chart)

