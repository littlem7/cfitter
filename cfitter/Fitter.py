"""
(based on the scipy.optimize module with curve_fit method)
"""
from scipy.optimize import curve_fit
import cfitter.DataGenerator as DG
import os
from array import *


def fit_data_from_file(name_of_file, function):
    """
    :param name_of_file: name of file without enlargement in which your data is
    :param function: function to fit
    :return: tuple with fitted parameters and cov
    """
    path_to_data_file = os.path.dirname(os.path.abspath(__file__)) + "/../data_base/"
    data_from_file = DG.read_form_csv(path_to_data_file + name_of_file + '.csv')
    if data_from_file:
        x_data = array('f', data_from_file[0])
        y_data = array('f', data_from_file[1])
        return curve_fit(function, x_data, y_data)
