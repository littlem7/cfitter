"""
(statistical analysis engine, should implement the χ 2 test to check the quality of your
fitting procedure)
"""
import cfitter.DataGenerator as DG
import os
from scipy.stats import chisquare
from array import *


def chi_square_test(file_with_data, function, statistical_significance = 0.05):
    """
    :param file_with_data: name of file without enlargement in which data is
    :param function: function to fit
    :param statistical_significance: statistic parameter
    :return: tuple with score of chi square test and p value
    """
    path_to_data_file = os.path.dirname(os.path.abspath(__file__)) + "/../data_base/"
    observed_data = DG.read_form_csv(path_to_data_file + file_with_data + '.csv')

    chi_square = chisquare(array('f', observed_data[1]), array('f', function(observed_data[0])))[0]
    p_value = chisquare(array('f', observed_data[1]), array('f', function(observed_data[0])))[1]
    print('Chi-square: %0.1f' % chi_square)
    if chi_square > 0:
        if 1 - p_value <= statistical_significance:
            print('Fitting is right with %0.1f %%' % (p_value*100))
        else:
            print('Fitting is wrong with %0.1f %%' % (100-(p_value*100)))
    else:
        if p_value <= statistical_significance:
            print('Fitting is right with %0.1f %%' % (100-(p_value*100)))
        else:
            print('Fitting is wrong with %0.1f %%' % (p_value*100))
    return chi_square, p_value

