"""
(for visualization)
"""
import matplotlib.pylab as plt
import os
import cfitter.DataGenerator as DG


def make_plot_from_file(name_of_file, show = False, x_label = 'x_time [sec]', y_label = 'y value []', name_of_chart = 'name_of_chart'):
    """
    :param name_of_file: name of file without enlargement in which data is
    :param show: correlated with show plot with data
    :param x_label: x label on plot
    :param y_label: y label on plot
    :param name_of_chart: name of plot
    """
    path_to_data_file = os.path.dirname(os.path.abspath(__file__)) + "/../data_base/"
    data_to_plot = DG.read_form_csv(path_to_data_file + name_of_file + '.csv')
    if data_to_plot:
        fig = plt.gcf()
        fig.clf()
        fig.canvas.set_window_title(name_of_chart)
        plt.plot(data_to_plot[0], data_to_plot[1], '.')
        plt.xlabel(x_label)
        plt.ylabel(y_label)
        if show:
            plt.show()
    else:
        print('File ' + name_of_file + " doesn't exist!")


def make_plot_with_function(name_of_file, function, show = False, x_label = 'x value []', y_label = 'y value []', name_of_chart = 'name_of_chart'):
    """
    :param name_of_file: name of file without enlargement in which data is
    :param function: function to fit
    :param show: correlated with show plot with data and fitted function
    :param x_label: x label on plot
    :param y_label: y label on plot
    :param name_of_chart: name of plot
    """
    make_plot_from_file(name_of_file, False, x_label, y_label, name_of_chart)
    path_to_data_file = os.path.dirname(os.path.abspath(__file__)) + "/../data_base/"
    data_to_plot = DG.read_form_csv(path_to_data_file + name_of_file + '.csv')
    if data_to_plot:
        plt.plot(data_to_plot[0], function(data_to_plot[0]))
        if show:
            plt.show()

