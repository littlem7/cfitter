"""
(the code is flexible enough to be able to model different noise level etc.)
"""
import csv
import os
import numpy as np


def generate_to_file(name_of_file, function, max_x_of_experiment = 10, frequency = 1, noise_max = 0):
    """
    :param name_of_file: name of file without enlargement in which can save data
    :param function: function to generate data
    :param max_x_of_experiment: positive number
    :param frequency: frequency of measurements; positive value
    :param noise_max: represent maximum of noise; positive value
    """
    if check_values(max_x_of_experiment, frequency, noise_max):
        path_to_data_file = os.path.dirname(os.path.abspath(__file__)) + "/../data_base/"
        with open(path_to_data_file + name_of_file + '.csv', 'w+') as csvfile:
            writer = csv.writer(csvfile, delimiter='#', quotechar=' ', quoting=csv.QUOTE_MINIMAL)
            single = 0
            while single < max_x_of_experiment:
                y_with_noise = make_some_noise(function(single), noise_max)
                writer.writerow([single, y_with_noise])
                single += frequency


def check_values(max_x_of_experiment, frequency, noise_max):
    """
    :param max_x_of_experiment: should be positive number
    :param frequency: should be positive number
    :param noise_max: should be positive number
    :return: True if parameters are ok' False if not
    """
    if max_x_of_experiment < 0:
        print('Incorrect time of experiment')
        return False
    elif frequency < 0:
        print('Incorrect frequency')
        return False
    elif noise_max < 0:
        print('Incorrect noise_max')
        return False
    else:
        return True


def make_some_noise(value, noise):
    """
    :param value: value without noise
    :param noise: degree of noise
    :return: value with some noise
    """
    random_noise = np.random.exponential(noise)
    random_sign = np.random.choice(['+', '-'])
    if random_sign == '+':
        return value + random_noise
    else:
        return value - random_noise


def read_form_csv(name_of_file, delimiter = '#', quotechar = ' '):
    """
    :param name_of_file: name of file without enlargement in which data is
    :param delimiter: delimiter in data file
    :param quotechar: quotechar in data file
    :return: x_data and y_data when you can read; None if not
    """
    x_data = []
    y_data = []
    if os.path.isfile(name_of_file):
        with open(name_of_file, 'r') as csvfile:
            reader = csv.reader(csvfile, delimiter=delimiter, quotechar=quotechar)
            for row in reader:
                try:
                    x_data.append(float(row[0]))
                except ValueError:
                    x_data.append(0)
                try:
                    y_data.append(float(row[1]))
                except ValueError:
                    y_data.append(0)
            return x_data, y_data
    else:
        print('File ' + name_of_file + " doesn't exist!")
        return None
