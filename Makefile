test:
	python3 -W ignore -m pytest tests/

device1:
	python3 -W ignore device1.py

device2:
	python3 -W ignore device2.py

device3:
	python3 -W ignore device3.py

clean:
	rm data_base/*.csv

.PHONY: test device1 device2 device3 clean

