"""
(examples of physical devices uses cfitter module)
"""
import cfitter.DataGenerator as DG
import cfitter.Approximator as A
import numpy as np
from scipy import signal


def water_level_of_the_vistula(proper_level, num_of_measurements, noise = 1.0, name_of_file = 'water_level_of_the_vistula'):
    """
    :param proper_level: proper lever of Vistula
    :param num_of_measurements: number of measurements (should be positive value)
    :param noise: noise degree
    :param name_of_file: name of file without enlargement in which data can be saved
    """
    def vistula_function(x):
        rand = np.random.choice(['rainy', 'not_rainy'])
        if rand == 'rainy':
            return proper_level + np.random.exponential(noise)
        else:
            return proper_level - np.random.exponential(noise)
    DG.generate_to_file(name_of_file, vistula_function, num_of_measurements, 1, noise)
    x_label = 'days od measurements [day]'
    y_label = 'level [m]'
    main_title = 'Level of water of Vistula'
    A.approximate_with_all(name_of_file, True, x_label, y_label, main_title)


def age_of_death_among_women(death_per_day = 100000, max_age = 101, noise = 1000, name_of_file = 'age_of_death_among_women'):
    """
    :param death_per_day: represent statistic number of women deaths per day
    :param max_age: represent max women age
    :param noise: noise degree
    :param name_of_file: name of file without enlargement in which data can be saved
    """
    gauss = signal.gaussian(max_age, std=20)

    def death_function(x):
        return gauss[x]*(0.8*death_per_day)
    DG.generate_to_file(name_of_file, death_function, max_age, 7, noise)
    x_label = 'age of women [years]'
    y_label = 'number of deaths per day [num]'
    main_title = 'Age of death among women'
    A.approximate_with_all(name_of_file, True, x_label, y_label, main_title)


def propagation_of_sound_in_air(max_amplitude = 0.5, time = 30, noise = 0.05, attenuation_factor = 1, name_of_file = 'propagation of sound in air'):
    """
    :param max_amplitude: represent max amplitude of wave
    :param time: represent time of experiment
    :param noise: noise degree
    :param attenuation_factor: physical parameter
    :param name_of_file: name of file without enlargement in which data can be saved
    """
    def sound_function(x):
        return (max_amplitude * np.exp(x * noise * (-1))) * np.sin(x)

    DG.generate_to_file(name_of_file, sound_function, time, 0.1, noise)
    x_label = 'time [ms]'
    y_label = 'amplitude [mm]'
    main_title = 'Propagation of sound in air'
    A.approximate_with_all(name_of_file, True, x_label, y_label, main_title)
