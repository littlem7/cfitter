from setuptools import setup

setup(
    name='cfitter',
    version='1.0',
    description='Curve Fitter',
    author='Anna Bogusz',
    author_email='anna.bogusz@fis.agh.edu.pl',
    packages=[
        'cfitter',
    ],
    setup_requires=[
        'pytest-runner',
    ],
    tests_require=[
        'pytest'
    ],
    install_requires=[
        'pytest',
        'matplotlib',
        'scipy',
    ],
)
