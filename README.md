# Curve fitter #

### cfitter ###
consists of:

* Data generator (this module uses .csv file to save (and read) the data)
* Fitter (this module uses scipy.optimize)
* StatAnalyser (this module implement χ2 test to check quality of fitting)
* Plotter (this module uses matplotlib.pylab for visualization)
* Approximator ( this module contains some functions to approximate and present using all modules)
* MyDevices (implements some physical devices; each of them uses cfitter functions)

##makefile commands##


```
#!python

make test
```
launches all test by pytest

```
#!python

make device1
```
generate, approximate and plot for water level of the Vistula with proper level 120 [cm]

```
#!python

make device2
```
generate, approximate and plot for age of death among women (gaussian)
```
#!python

make device3
```
generate, approximate and plot for propagation of sound in air (with attenuation factor)

```
#!python

make clean
```
removes all .csv files from data_base