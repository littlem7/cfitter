import cfitter.StatAnalyser as SA
import numpy as np
import cfitter.Fitter as F


def test1_test_chi_square():
    name_of_file = 'data_mine'

    def function_to_fit(x, a, b, c):
        return a*np.cos(x - b) + c

    values = F.fit_data_from_file(name_of_file, function_to_fit)[0]

    def fitted_function(x):
        return function_to_fit(x, values[0], values[1], values[2])

    SA.chi_square_test(name_of_file, fitted_function)
