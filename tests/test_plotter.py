import cfitter.Plotter as P
import numpy as np
import cfitter.Fitter as F
from array import *


def test1_plot_from_file():
    name_of_file = 'data'
    P.make_plot_from_file(name_of_file)


def test2_plot_from_file_witch_doesnt_exist():
    name_of_file = 'not_exist_data'
    P.make_plot_from_file(name_of_file)


def test3_plot_from_file_with_fitted_function():
    name_of_file = 'data'

    def function_to_fit(x, a, b, c):
        return a*np.sin(x-b) + c

    values = F.fit_data_from_file(name_of_file, function_to_fit)[0]

    def fitted_function(x):
        return function_to_fit(x, values[0], values[1], values[2])

    P.make_plot_with_function(name_of_file, fitted_function)


def test4_plot_from_file_with_another_fitted_function():
    name_of_file = 'data_mine'

    def function_to_fit(x, a, b, c):
        return a*np.cos(x - b) + c

    values = F.fit_data_from_file(name_of_file, function_to_fit)[0]

    def fitted_function(x):
        return function_to_fit(x, values[0], values[1], values[2])

    P.make_plot_with_function(name_of_file, fitted_function)
