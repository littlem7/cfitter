import cfitter.DataGenerator as DG
import math
import os


def test1_data_generator_with_correct_values():
    name_of_file = 'data'
    noise_max = 2.3

    def sample_function(x):
        return math.sin(x) + 10

    time_of_experiment = 10
    frequency_of_measurement = 0.01
    DG.generate_to_file(name_of_file, sample_function, time_of_experiment, frequency_of_measurement, noise_max)


def test1b_data_generator_with_correct_values():
    name_of_file = 'data_mine'
    noise_max = 2.3

    def sample_function(x):
        return math.sin(x) + 10

    time_of_experiment = 10
    frequency_of_measurement = 0.01
    DG.generate_to_file(name_of_file, sample_function, time_of_experiment, frequency_of_measurement, noise_max)


def test2_data_generator_with_incorrect_values():
    name_of_file = 'data'
    noise_max = 0.1

    def sample_function(x):
        return math.sin(x)

    time_of_experiment = -10         # [sec]
    frequency_of_measurement = 0.01    # [sec]
    DG.generate_to_file(name_of_file, sample_function, time_of_experiment, frequency_of_measurement, noise_max)


def test3_read_data_from_file():
    path_to_data_file = os.path.dirname(os.path.abspath(__file__)) + "/../data_base/"
    name_of_file = 'data.csv'
    print(DG.read_form_csv(path_to_data_file + name_of_file, '#', ' '))
    print(DG.read_form_csv('bad_data.csv'))


