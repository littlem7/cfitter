import cfitter.Fitter as F
import numpy as np


def test1_fit_to_data_from_file():
    name_of_file = 'data'

    def function_to_fit(x, a, b, c):
        return a*np.sin(x-b) + c

    F.fit_data_from_file(name_of_file, function_to_fit)


